<?php include "inc/header.php" ?>

<div class="container">
    <h2>2 colonnes</h2>
    <div class="row show-grid">
        <div class="span6">Column 1</div>
        <div class="span6">Column 2</div>
    </div>

    <h2>3 colonnes</h2>
    <div class="row show-grid">
        <div class="span4">Column 1</div>
        <div class="span4">Column 2</div>
        <div class="span4">Column 3</div>
    </div>

    <h3>4 colonnes</h3>
    <div class="row show-grid">
        <div class="span3">Column 1</div>
        <div class="span3">Column 2</div>
        <div class="span3">Column 3</div>
        <div class="span3">Column 4</div>
    </div>

    <h3>Une combinaison de plusieurs blocs</h3>
    <div class="row show-grid">
        <div class="span12">Meta header </div>
    </div>
    <div class="row show-grid">
        <div class="span4">Un menu</div>
        <div class="span8">le contenu</div>
    </div>
    <div class="row show-grid">
        <div class="span4">blah</div>
        <div class="span4">foo Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore nobis inventore nostrum odit distinctio doloribus obcaecati modi possimus atque iure harum ad quas fugit nihil voluptate ipsum adipisci! Excepturi maiores.</div>
        <div class="span4">bar</div>
    </div>
    <div class="row show-grid">
        <div class="span8">des nouvelles</div>
        <div class="span4">des activités Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt repudiandae commodi adipisci eius fugit similique cum architecto delectus nemo a cupiditate ab dicta nostrum ex nobis rerum quas quisquam fugiat.</div>
    </div>
    <div class="row show-grid">
        <div class="span12">Meta footer</div>
    </div>
</div>

<?php include "inc/footer.php" ?>