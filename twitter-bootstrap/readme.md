# Travailler efficacement avec Twitter Bootstrap

Une présentation pour les intégrateurs et les développeurs web de chez SFL

## Qu'est-ce que c'est ?

C'est un framework front-end (HTML/CSS/JS) pour accélérer le rythme de notre travail d'intégration.

Il inclut:

* une base visuelle attrayante pour démarrer un projet
* des styles de base pour la typographie (H1, H2, Hx, P, UL LI)
* des styles de base pour les tables
* des styles de base pour les formulaires (tous les types de champ de saisie)
* des styles de base pour les boutons (bouton de type INPUT ou balise A)
* des styles de base pour les icônes
* un système de grille responsive
* des composantes HTML complexes (fil d'ariane, onglet, pagination, alertes, barre de progression)
* et des plugins javascript (Modal, Tooltip, Carousel) qui dépendent de jQuery.

Il est à noté que nous pouvons personnaliser TB à notre guise en ayant accès au
fichier .less du projet ou via leur [outil en ligne](http://twitter.github.com/bootstrap/customize.html).

Il existe également des [ressources en ligne qui répertorie des thèmes pour Twitter bootstrap](https://wrapbootstrap.com/).


## Pourquoi l'utiliser ?

Je vois principalement TB comme étant une fondation possible pour la
standardisation de notre travail d'intégration pour un squelettevisuel unifié et
personnalisable.

La maîtrise de ces règles et structures se fait sans heurt et plutôt rapidement.

En arrivant dans un projet monté avec TB, nous n'aurons pas besoin de chercher
longtemps pour savoir comment ajouter/modifier/supprimer des éléments du GUI.

## Faciliter la mémorisation et le travail avec un plugin pour Sublime Text 2

[Sublime Twitter bootstrap snippets](https://github.com/devtellect/sublime-twitter-bootstrap-snippets>).

Ce plugin nous permet d'utiliser des code snippets qui nous permet de monter
une structure html rapidement:

**tbbutton** donne:

	<button class="btn" type="submit">Button</button>

**tbbreadcrumb** donne:


	<ul class="breadcrumb">
		<li>
			<a href="#">Home</a> <span class="divider">/</span>
		</li>
		<li>
			<a href="#">Library</a> <span class="divider">/</span>
		</li>
		<li class="active">
			<a href="#">Data</a>
		</li>
	</ul>


## Exemples d'utilisation

Même si la documentation est complète, c'est toujours utile de voir des démonstrations live !

Voici donc ce que je vais aborder comme sujets/procédures.

### Débuter un projet

**Fichier**: starter.php

Le grid (responsive ou non)


### **Fichier**: grid.php

Les formulaires


### **Fichier**: forms.php

Les boutons


### **Fichier**: buttons.php

Les tables


### **Fichier**: tables.php

Composantes HTML


### **Fichier**: components.php

Composantes JS


### **Fichier**: components-js.php

## Alternatives à TB

- [Zurb Foundation](http://foundation.zurb.com/)
- [Easy Framework](http://easyframework.com/)
- [G5 Framework](http://framework.gregbabula.info/)
- [Skeleton](http://www.getskeleton.com/)
- [16 Useful Responsive CSS Frameworks And Boilerplates](http://www.smashingapps.com/2012/12/26/16-useful-responsive-css-frameworks-and-boilerplates.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3a%20SmashingApps%20%28Smashing%20Apps%29)