# PatternLab

## a.k.a. La magie des systèmes de design atomique

---

<img src="assets/img/wat.png" alt="Wat" />

---

# Qu'est-ce que c'est ?

Un générateur de guide de style visuel *et* fonctionnel pour nos projets

---

# Ce qu'il n'est pas

* Un cadriciel d'interface usager (ex: Twitter Bootstrap, Zurb Foundation)
* Une solution inflexible

---

# Pourquoi l'utiliser ?

* Besoin d'un système modulaire et réutilisable
* Point de référence commun pour tous les métiers qui gravitent autour d'un projet web

---

# Comment ça fonctionne ?

---

## Design atomique

Des **atomes** qui forment des **molécules**, qui forment des **organismes**, qui forment des **gabarits**, qui forment des **pages** !

---

## Qu'utilises-t-on pour créer ces composantes ?

* Un système de gabarit nommé *Mustache* pour le HTML
* CSS, JS, images et polices
* Du JSON pour des données fictives

---

## Mustache ?

    !javascript
    { "name": "bareine" }

combiné à:

    !html
    <h1>Salut {{name}}</h1>

donne:

    !html
    <h1>Salut bareine</h1>

---

    !javascript
    {
        food: [
            "la poutine",
            "les croustilles",
            "et la pizza"
        ]
    }

combiné à

    !html
    <h2>J'aime:</h2>
    <ul>
        {{#food}}
        <li>{{.}}</li>
        {{/food}}
    </ul>

donne ?

---

    !html
    <h2>J'aime:</h2>
    <ul>
        <li>la poutine</li>
        <li>les croustilles</li>
        <li>et la pizza</li>
    </ul>

---

# Structure de dossier d'un projet sous Patternlab

---

### Générer le site statique

    !bash
    $ cd source
    $ php core/builder.php -g

### Générer le site lors d'une modification (watch)

    !bash
    $ php core/builder.php -w

---

# Demain ?

Tentative de reproduire le guide de style du site de recrutement SFL en groupe !