# Comment créer la présentation ?

    $ virtualenv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt
    $ landslide presentation.cfg

Le fichier **presentation.html** sera généré à partir du fichier **presentation.md**.

Vous n'avez qu'à l'ouvrir avec votre navigateur favori.